#ifndef PLAYER_H_
#define PLAYER_H_

#include "Ogre.h"

#include "bullet/btBulletCollisionCommon.h"
#include "bullet/btBulletDynamicsCommon.h"

using namespace Ogre;

class Player
{
private:
  SceneNode* boxSceneNode;
  Entity* box;
  Vector3 meshBoundingBox;

  btCollisionShape* colShape;
  btRigidBody* body;
  btDiscreteDynamicsWorld* dynamicsWorld;

  float forwardForce;
  float turningForce;
  btScalar linearDamping;
  btScalar angularDamping;

public:
  Player();
  ~Player();

  void createMesh(SceneManager* scnMgr);

  void attachToNode(SceneNode* parent);

  void setScale(float x, float y, float z);

  void setRotation(Vector3 axis, Radian angle);
  
  void setPosition(float x, float y, float z);

  void boundingBoxFromOgre();

  void createRigidBody(float mass);

  void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);

  void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);

  void update();

  void forward();

  void turnRight();

  void turnLeft();

};


#endif
