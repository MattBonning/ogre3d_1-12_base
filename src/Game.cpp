#include <exception>
#include <iostream>
#include "Game.h"

Game::Game() : ApplicationContext("st20146881 Simple Game")
{
    dynamicsWorld = NULL;

    wDown = true; //I have spoken to Wenshu about the user input not working. I have set these bools in order to simulate the player movement.
    dDown = false;
	aDown = false;
}

Game::~Game()
{

    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
        btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);

        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }

        dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }

    for (int j = 0; j < collisionShapes.size(); j++)
    {
        btCollisionShape* shape = collisionShapes[j];
        collisionShapes[j] = 0;
        delete shape;
    }

    delete dynamicsWorld;

    delete solver;

    delete overlappinbgPairCache;

    delete dispatcher;

    delete collisionConfiguration;

    collisionShapes.clear();
}

void Game::setup()
{
    ApplicationContext::setup();

    addInputListener(this);

    Root* root = getRoot();
    scnMgr = root->createSceneManager();

    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    bulletInit();

    setupCamera();

    setupFloor();

    setupLights();

    setupBoxMesh();

    setupPlayer();
}

void Game::setupCamera()
{

    Camera* cam = scnMgr->createCamera("myCam");

    cam->setNearClipDistance(5);

    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(500, 400, 600);
    camNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
    camNode->attachObject(cam);

    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0, 25, 0));

    cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
}

void Game::bulletInit()
{
    collisionConfiguration = new btDefaultCollisionConfiguration();

    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    overlappinbgPairCache = new btDbvtBroadphase();

    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappinbgPairCache, solver, collisionConfiguration);

    dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

void Game::setupPlayer()
{
    SceneNode* sceneRoot = scnMgr->getRootSceneNode();
    float mass = 1.0f;

    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    Radian rads(Degree(25.0));

    player = new Player();
    player->createMesh(scnMgr);
    player->attachToNode(sceneRoot);

    player->setRotation(axis, rads);
    player->setPosition(20.0f, 20.0f, 20.0f);

    player->createRigidBody(mass);
    player->addToCollisionShapes(collisionShapes);
    player->addToDynamicsWorld(dynamicsWorld);
}

void Game::setupBoxMesh()
{
    Entity* box = scnMgr->createEntity("cube.mesh");
    box->setCastShadows(true); //Shadows left on the cube for 'decorative' purposes.

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(box);

    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    Radian rads(Degree(60.0));

    Quaternion quat(rads, axis);

    thisSceneNode->setScale(1.0, 1.0, 1.0);

    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB();

    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);

    thisSceneNode->setPosition(0, 200, 0);

    Vector3 meshBoundingBox(b.getSize());

    if (meshBoundingBox == Vector3::ZERO)
    {
        std::cout << "bounding voluem size is zero." << std::endl;
    }

    btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
    std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;

    collisionShapes.push_back(colShape);

    btTransform startTransform;
    startTransform.setIdentity();

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(1.f);

    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
    {
        colShape->calculateLocalInertia(mass, localInertia);
    }

    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    body->setUserPointer((void*)thisSceneNode);

    dynamicsWorld->addRigidBody(body);
}

void Game::setupFloor()
{
    Plane plane(Vector3::UNIT_Y, 0);

    MeshManager::getSingleton().createPlane(
        "ground", RGN_DEFAULT,
        plane,
        3000, 3000, 20, 20,
        true,
        1, 5, 5,
        Vector3::UNIT_Z);

    Entity* groundEntity = scnMgr->createEntity("ground");

    groundEntity->setCastShadows(false); //Shadows off for the ground entity.

    groundEntity->setMaterialName("Examples/Rockwall");

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(groundEntity);

    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(1500.), btScalar(50.), btScalar(1500.)));

    collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();

    Vector3 pos = thisSceneNode->_getDerivedPosition();

    groundTransform.setOrigin(btVector3(pos.x, pos.y - 50.0, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(0.);

    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        groundShape->calculateLocalInertia(mass, localInertia);

    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    dynamicsWorld->addRigidBody(body);
}

bool Game::frameStarted(const Ogre::FrameEvent& evt)
{
    ApplicationContext::frameStarted(evt);
    if (this->dynamicsWorld != NULL)
    {
        if (wDown) 
        {
            player->forward(); //Linking the forward function to wDown on the keyboard.
        }

        if (dDown)
        {
            player->turnRight(); //Linking turnRight function to dDown on the keyboard.
        }

		if (aDown)
		{
			player->turnLeft(); //Linking turnLeft function to aDown on the keyboard.
		}

		dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);

        for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
        {
            btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
            btRigidBody* body = btRigidBody::upcast(obj);
            btTransform trans;

            if (body && body->getMotionState())
            {
                body->getMotionState()->getWorldTransform(trans);

                void* userPointer = body->getUserPointer();

                if (userPointer == player)
                {
                
                }

                else //This is just to keep the other objects working.
                {
                    if (userPointer)
                    {
                        btQuaternion orientation = trans.getRotation();
                        Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(userPointer);
                        sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                        sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
                    }
                }
            }

            else
            {
                trans = obj->getWorldTransform();
            }
        }

        player->update();
    }

    return true;
}

bool Game::frameEnded(const Ogre::FrameEvent& evt)
{
    if (this->dynamicsWorld != NULL)
    {
        dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);
    }
    return true;
}

void Game::setupLights()
{
    scnMgr->setAmbientLight(ColourValue(0, 0, 0));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

    Light* spotLight = scnMgr->createLight("SpotLight");

    spotLight->setDiffuseColour(0, 0, 1.0);
    spotLight->setSpecularColour(0, 0, 1.0);
    spotLight->setType(Light::LT_SPOTLIGHT);
    spotLight->setSpotlightRange(Degree(35), Degree(50));

    SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->setDirection(-1, -1, 0);
    spotLightNode->setPosition(Vector3(200, 200, 0));

    spotLightNode->attachObject(spotLight);

    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(0.4, 0, 0));
    directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));

    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));

    Light* pointLight = scnMgr->createLight("PointLight");

    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);

    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    pointLightNode->setPosition(Vector3(0, 150, 250));

    pointLightNode->attachObject(pointLight);

}

bool Game::keyPressed(const KeyboardEvent& evt)
{
    std::cout << "Got key down event" << std::endl;
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }

    if (evt.keysym.sym == 'w') //Setting wDown as true if the w key is pressed down.
    {
        wDown = true;
    }

    if (evt.keysym.sym == 'd') //Setting dDown as true if the d key is pressed down.
    {
        dDown = true;
    }

	if (evt.keysym.sym == 'a') //Setting aDown as true if the a key is pressed down.
	{
		aDown = true;
	}

    return true;
}

bool Game::keyReleased(const KeyboardEvent& evt)
{
    std::cout << "Got key up event" << std::endl;

    if (evt.keysym.sym == 'w') //Setting wDown as false if the w key is released.
    {
        wDown = false;
    }

    if (evt.keysym.sym == 'd') //Setting dDown as false if the d key is released.
    {
        dDown = false;
    }

	if (evt.keysym.sym == 'a') //Setting aDown as false if the a key is released.
	{
		aDown = false;
	}

    return true;
}

bool Game::mouseMoved(const MouseMotionEvent& evt)
{
    std::cout << "Got Mouse" << std::endl;
    return true;
}
