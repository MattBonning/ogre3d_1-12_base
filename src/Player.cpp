#include "Player.h"

Player::Player()
{
  boxSceneNode = nullptr;
  box = nullptr;
  Vector3 meshBoundingBox(0.0f,0.0f,0.0f);

  colShape = nullptr;
  dynamicsWorld = nullptr;

  forwardForce = 100.0f;
  turningForce = 20.0f;
  linearDamping = 0.2f;
  angularDamping = 0.8f;
}

Player::~Player()
{

}

void Player::createMesh(SceneManager* scnMgr)
{
  box = scnMgr->createEntity("cube.obj"); //A .obj that I made in 3ds max, just a simple place holder cube.

  box->setCastShadows(true);

  //box->setMaterialName("green.jpg"); //This sets the texture for the player, however I have opted to use the pink colour on the obj, as it is brighter.
}

void Player::attachToNode(SceneNode* parent)
{
  boxSceneNode = parent->createChildSceneNode();
  boxSceneNode->attachObject(box);
  boxSceneNode->setScale(1.0f,1.0f,1.0f);
  boundingBoxFromOgre();
}

void Player::setScale(float x, float y, float z)
{
    boxSceneNode->setScale(x,y,z);
}

void Player::setRotation(Vector3 axis, Radian rads)
{
  Quaternion quat(rads, axis);
  boxSceneNode->setOrientation(quat);
}

void Player::setPosition(float x, float y, float z)
{
  boxSceneNode->setPosition(-250,200,-900);
}

void Player::boundingBoxFromOgre()
{
  boxSceneNode->_updateBounds();
  const AxisAlignedBox& b = boxSceneNode->_getWorldAABB();
  Vector3 temp(b.getSize());
  meshBoundingBox = temp;
}

void Player::createRigidBody(float bodyMass)
{
  colShape = new btBoxShape(btVector3(meshBoundingBox.x/2.0f, meshBoundingBox.y/2.0f, meshBoundingBox.z/2.0f));

  btTransform startTransform;
  startTransform.setIdentity();

  Quaternion quat2 = boxSceneNode->_getDerivedOrientation();
  startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

  Vector3 pos = boxSceneNode->_getDerivedPosition();
  startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

  btScalar mass(bodyMass);

  bool isDynamic = (mass != 0.f);

  btVector3 localInertia(0, 0, 0);
  if (isDynamic)
  {
      colShape->calculateLocalInertia(mass, localInertia);
  }

  btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
  btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
  body = new btRigidBody(rbInfo);

  //Set the linear and angular damping
  //I'm using this to bring the object to rest when moving.
  //An alternative would be to use friciton for the collison.
  //No good for hovering stuff though.
  body->setDamping(linearDamping,angularDamping);

  body->setUserPointer((void*)this);
}

void Player::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
  collisionShapes.push_back(colShape);
}

void Player::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
  this->dynamicsWorld = dynamicsWorld;
  dynamicsWorld->addRigidBody(body);
}

void Player::update()
{
  btTransform trans;

  if (body && body->getMotionState())
  {
    body->getMotionState()->getWorldTransform(trans);
    btQuaternion orientation = trans.getRotation();

    boxSceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
    boxSceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
  }
}

void Player::forward() //Basic function to move the player forward.
{
    btVector3 fwd(0.0f,0.0f,forwardForce);
    btVector3 push;

    btTransform trans;

    if (body && body->getMotionState())
    {
        body->getMotionState()->getWorldTransform(trans);
        btQuaternion orientation = trans.getRotation();

        push = quatRotate(orientation, fwd);

        body->activate();

        body->applyCentralForce(push);
    }
}

void Player::turnRight() //Basic function to turn the player right.
{
    btVector3 right(turningForce,0.0f,0.0f);
    btVector3 turn;

    btTransform trans;

    if (body && body->getMotionState())
    {
        body->getMotionState()->getWorldTransform(trans);
        btQuaternion orientation = trans.getRotation();

        btVector3 front(trans.getOrigin());

        front += btVector3(0.0f,0.0f,meshBoundingBox.z/2);

        turn = quatRotate(orientation, right);

        if(body->getLinearVelocity().length() > 0.0f)
            body->applyForce(turn,front);
    }
}

void Player::turnLeft() //Basic function to turn the player left.
{
	btVector3 left(-turningForce, 0.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		btVector3 front(trans.getOrigin());

		front += btVector3(0.0f, 0.0f, meshBoundingBox.z / 2);

		turn = quatRotate(orientation, left);

		if (body->getLinearVelocity().length() > 0.0f)
			body->applyForce(turn, front);
	}
}